XpertGroup Technical Test By Al3xCubillos
------------------------

Repositorio que contiene la implementación en C# del reto 'Cube Summation' planteado en https://www.hackerrank.com/challenges/cube-summation/problem



CAPAS DE LA APLICACIÓN:
=======

Dado que el reto en cuanto a componentes es bastabte simple decidí escribir una libreria **Core** en la cual se centra toda la logica y se usa inyeccion de dependencias (SimpleInjector).

Desde la capa de presentacion, en este caso una aplicacion de consola, se puede interactuar con la capa **Core** la cual es perfectamente usable en cualquier tipo de app (web, desktop, etc).

Se incluyen pruebas unitarias y pruebas de integracion. Para ello se usan **Xunit** y **Moq**

La inyeccion de dependencias se meneja en la capa **Core** y desde afuera nadie sabe que esto funciona asi.


CLASES (principales):
=======

 - **CubeSummationManagerFactory:** Una simple y clasica Factoria la cual entrega una instancia de tipo **ICubeSummationManager** que inicializa el contenedor de inyeccion de dependencias. **ICubeSummationManager** implementa **IDisposable**.

 - **ICubeSummationManager** expone el metodo **BuildCubeTestCases** el cual construye una instancia de **ICubeTestCases** por medio del Builder **ICubeTestBuilder**

 - **ICubeTestCases** encargada del menejo de los casos de prueba. Se mueve al siguiente caso de prueba usando el metodo **MoveNext(...)** casi como lo hace un **IEnumerator**. Tambien expone una propiedad **Current** la cual accede al **ICube** actual.

 - **ICubeDataValidation** encargada de las diferentes validaciones.

 - **ICube** contiene la logica refente al Cubo, como actualizar un valor o cosultar un valor (UPDATE & QUERY).


PREGUNTAS:
=======

**¿En qué consiste el principio de responsabilidad única?**

Mantener una alta cohesión, es decir, mantener ‘unidas’ funcionalidades que estén relacionadas entre sí y mantener fuera aquello que no esté relacionado.
Mantener un bajo acoplamiento, es decir, reducir al máximo posible el grado de la relación de un clase o módulo con el resto, para favorecer crear 
código más fácilmente mantenible, extensible y testeable.

**¿Qué es código limpio?**

Código limpio se refiere a un software (piezas de software) que sean fácil de leer, escribir y mantener. Adicionalmente es importante que esas piezas 
de software tengan la habilidad de ser fácilmente extendibles y refactorizadas