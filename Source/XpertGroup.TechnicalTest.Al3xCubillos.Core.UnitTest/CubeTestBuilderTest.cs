﻿using Moq;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;
using Xunit;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.UnitTest
{
    public class CubeTestBuilderTest
    {
        #region [ Setup ]

        private const long RANGE_VALIDATION_MAX_VALUE = 10;
        private readonly Mock<ICubeDataValidation> _mockOfCubeDataValidation;
        internal CubeTestBuilder InstanceToTest { get; }


        public CubeTestBuilderTest()
        {
            _mockOfCubeDataValidation = new Mock<ICubeDataValidation>();

            _mockOfCubeDataValidation
                .Setup(x => x.TestCases(It.IsAny<long>()))
                .Returns((long value) => RangeValidation(value));


            InstanceToTest = new CubeTestBuilder(_mockOfCubeDataValidation.Object);
        }


        private bool RangeValidation(long value)
        {
            return 1 <= value && value <= RANGE_VALIDATION_MAX_VALUE;
        }

        #endregion

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1000)]
        public void Build_InvalidParams_ThrowsOutOfRangeValueCubeException(long testCases)
        {
            Assert.Throws<OutOfRangeValueCubeException>(() =>
                InstanceToTest.Build(testCases)
            );
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(4)]
        public void Build_ValidParams_RestulIsNotNull(long testCases)
        {
            var cubeTestCases = InstanceToTest.Build(testCases);
            Assert.NotNull(cubeTestCases);
        }
    }
}
