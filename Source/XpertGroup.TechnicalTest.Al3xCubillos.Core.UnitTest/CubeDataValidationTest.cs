﻿using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;
using Xunit;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.UnitTest
{
    public class CubeDataValidationTest
    {
        #region [ Setup ]
        
        internal CubeDataValidation InstanceToTest { get; } 

        public CubeDataValidationTest()
        { 
            InstanceToTest = new CubeDataValidation();
        }

        #endregion


        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(25)]
        [InlineData(50)]
        public void TestCases_ValidValue_ResultIsTrue(long value)
        { 
            var result = InstanceToTest.TestCases(value);
            Assert.True(result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-10)]
        [InlineData(51)]
        public void TestCases_InvalidValue_ResultIsFalse(long value)
        {
            var result = InstanceToTest.TestCases(value);
            Assert.False(result);
        }



        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(50)]
        [InlineData(100)]
        public void CubeSize_ValidValue_ResultIsTrue(long value)
        {
            var result = InstanceToTest.CubeSize(value);
            Assert.True(result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-50)]
        [InlineData(101)]
        public void CubeSize_InvalidValue_ResultIsFalse(long value)
        {
            var result = InstanceToTest.CubeSize(value);
            Assert.False(result);
        }



        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        [InlineData(500)]
        [InlineData(1000)]
        public void Operations_ValidValue_ResultIsTrue(long value)
        {
            var result = InstanceToTest.Operations(value);
            Assert.True(result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-500)]
        [InlineData(1001)]
        public void Operations_InvalidValue_ResultIsFalse(long value)
        {
            var result = InstanceToTest.Operations(value);
            Assert.False(result);
        }



        [Theory]
        [InlineData(10, 1, 2)]
        [InlineData(10, 2, 4)]
        [InlineData(10, 5, 7)]
        [InlineData(10, 1, 10)]
        public void IndexesOfQuery_ValidValue_ResultIsTrue(long cubeSize, long index1, long index2)
        {
            var result = InstanceToTest.IndexesOfQuery(cubeSize, index1, index2);
            Assert.True(result);
        }

        [Theory]
        [InlineData(10, 10, 2)]
        [InlineData(10, 2, 1)]
        [InlineData(10, 15, 7)]
        [InlineData(10, 11, 15)]
        public void IndexesOfQuery_InvalidValue_ResultIsFalse(long cubeSize, long index1, long index2)
        {
            var result = InstanceToTest.IndexesOfQuery(cubeSize, index1, index2);
            Assert.False(result);
        }



        [Theory]
        [InlineData(1)]
        [InlineData(1000000)]
        [InlineData(1000000000)]
        [InlineData(-1000000000)]
        public void NewValue_ValidValue_ResultIsTrue(long value)
        {
            var result = InstanceToTest.NewValue(value);
            Assert.True(result);
        }

        [Theory]
        [InlineData(1000000001)]
        [InlineData(-1000000001)]
        [InlineData(long.MaxValue)]
        [InlineData(long.MinValue)]
        public void NewValue_InvalidValue_ResultIsFalse(long value)
        {
            var result = InstanceToTest.NewValue(value);
            Assert.False(result);
        }



        [Theory]
        [InlineData(10, 1)]
        [InlineData(10, 3)]
        [InlineData(10, 6)]
        [InlineData(10, 10)]
        public void NewValueIndex_ValidValue_ResultIsTrue(long cubeSize, long index)
        {
            var result = InstanceToTest.NewValueIndex(cubeSize, index);
            Assert.True(result);
        }

        [Theory]
        [InlineData(10, 0)]
        [InlineData(10, 11)]
        [InlineData(10, -1)]
        public void NewValueIndex_InvalidValue_ResultIsFalse(long cubeSize, long index)
        {
            var result = InstanceToTest.NewValueIndex(cubeSize, index);
            Assert.False(result);
        }
    }
}
