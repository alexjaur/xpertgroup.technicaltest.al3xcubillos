﻿using Moq;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;
using Xunit;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.UnitTest
{
    public class CubeTestCasesTest
    {
        #region [ Setup ]

        private const long RANGE_VALIDATION_MAX_VALUE = 10;
        private readonly Mock<ICubeDataValidation> _mockOfCubeDataValidation;
        internal CubeTestCases InstanceToTest { get; set; }


        public CubeTestCasesTest()
        {
            _mockOfCubeDataValidation = new Mock<ICubeDataValidation>();

            _mockOfCubeDataValidation
                .Setup(x => x.CubeSize(It.IsAny<long>()))
                .Returns((long value) => RangeValidation(value));

            _mockOfCubeDataValidation
                .Setup(x => x.Operations(It.IsAny<long>()))
                .Returns((long value) => RangeValidation(value));


            InstanceToTest = new CubeTestCases(_mockOfCubeDataValidation.Object, 1);
        }


        private bool RangeValidation(long value)
        {
            return 1 <= value && value <= RANGE_VALIDATION_MAX_VALUE;
        }

        #endregion

        [Theory]
        [InlineData(-1, 1)]
        [InlineData(1, -1)]
        [InlineData(1, 1000)]
        public void MoveNext_InvalidParams_ThrowsOutOfRangeValueCubeException(long cubeSize, long operations)
        {
            Assert.Throws<OutOfRangeValueCubeException>(() =>
                InstanceToTest.MoveNext((x) => {
                    x.CubeSize = cubeSize;
                    x.Operations = operations;
                })
            );
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(4)]
        public void Build_ValidParams_ExpectedCurrentValue(long testCases)
        {
            InstanceToTest = new CubeTestCases(_mockOfCubeDataValidation.Object, testCases);

            void CubeConfiguration(CubeData x)
            {
                x.CubeSize = 10;
                x.Operations = 5;
            }

            while (InstanceToTest.MoveNext(CubeConfiguration))
            {
                Assert.NotNull(InstanceToTest.Current);
            }

            Assert.Null(InstanceToTest.Current); 
        }
    }
}
