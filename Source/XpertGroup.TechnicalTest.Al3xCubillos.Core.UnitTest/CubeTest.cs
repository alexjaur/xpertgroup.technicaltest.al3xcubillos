﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;
using Xunit;
using Moq;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.UnitTest
{
    public class CubeTest
    {
        #region [ Setup ]

        private const long RANGE_VALIDATION_MAX_VALUE = 10;
        private readonly Mock<ICubeDataValidation> _mockOfCubeDataValidation;
        internal Cube InstanceToTest { get; }

        public CubeTest()
        {
            _mockOfCubeDataValidation = new Mock<ICubeDataValidation>();
            
            _mockOfCubeDataValidation
                .Setup(x => x.CanExecuteOperation(It.IsAny<long>(), It.IsAny<long>()))
                .Returns((long maxOperations, long currentOperations) => 1 <= maxOperations && currentOperations < maxOperations);

            _mockOfCubeDataValidation
                .Setup(x => x.NewValueIndex(It.IsAny<long>(), It.IsAny<long>()))
                .Returns((long cubeSize, long index) => 1 <= index && index <= cubeSize);

            _mockOfCubeDataValidation
                .Setup(x => x.NewValue(It.IsAny<long>()))
                .Returns((long value) => RangeValidation(value));

            _mockOfCubeDataValidation
                .Setup(x => x.IndexesOfQuery(It.IsAny<long>(), It.IsAny<long>(), It.IsAny<long>())) 
                .Returns((long cubeSize, long index1, long index2) => 1 <= index1 && index1 <= index2 && index2 <= cubeSize);


            InstanceToTest = new Cube(_mockOfCubeDataValidation.Object, 5, 5);
        }

        private bool RangeValidation(long value)
        {
            return 1 <= value && value <= RANGE_VALIDATION_MAX_VALUE;
        }

        #endregion

        [Fact]
        public void Update_OperationsExceeded_ThrowCubeException()
        {
            InstanceToTest.CurrentOperations = InstanceToTest.Operations;

            Assert.Throws<CubeException>(() =>
                InstanceToTest.Update(1, 1, 1, 1)
            );
        }

        [Theory]
        [InlineData(6, 1, 1, 1)]
        [InlineData(1, 6, 1, 1)]
        [InlineData(1, 1, 6, 1)]
        [InlineData(1, 1, 1, long.MaxValue)]
        [InlineData(1, 1, 1, long.MinValue)]
        public void Update_InvalidIndexesAndValue_ThrowOutOfRangeValueCubeException(long x, long y, long z, long value)
        {
            Assert.Throws<OutOfRangeValueCubeException>(() =>
                InstanceToTest.Update(x, y, z, value)
            );
        }
        
        [Theory]
        [InlineData(1, 1, 1, 1)]
        [InlineData(2, 1, 1, 2)]
        [InlineData(3, 5, 4, 5)]
        public void Update_ValidParameters_ExpectedResult(long x, long y, long z, long value)
        { 
            var expected = InstanceToTest.CurrentOperations + 1;

            InstanceToTest.Update(x, y, z, value);

            Assert.Equal(expected, InstanceToTest.CurrentOperations);
        }



        [Theory]
        [InlineData(1, true)]
        [InlineData(100, false)]
        public void CanExecuteNextOperation_ExpectedResult(long currentOperations, bool expectedResult)
        { 
            InstanceToTest.CurrentOperations = currentOperations;

            var result = InstanceToTest.CanExecuteNextOperation();

            Assert.Equal(expectedResult, result);
        }



        [Fact]
        public void Query_OperationsExceeded_ThrowCubeException()
        { 
            InstanceToTest.CurrentOperations = InstanceToTest.Operations;

            Assert.Throws<CubeException>(() =>
                InstanceToTest.Query(1, 1, 1, 1, 1, 1)
            );
        }

        [Theory]
        [InlineData(2, 1, 1, 1, 1, 1)]
        [InlineData(1, 6, 1, 1, 1, 1)]
        [InlineData(1, 1, 1, 1, 10, 1)]
        [InlineData(1, 1, 3, 1, 1, 1)]
        [InlineData(1, 1, 1, 1, 1, 7)]
        public void Query_InvalidIndexes_ThrowOutOfRangeValueCubeException(long x1, long y1, long z1, long x2, long y2, long z2)
        {
            Assert.Throws<OutOfRangeValueCubeException>(() =>
                InstanceToTest.Query(x1, y1, z1, x2, y2, z2)
            );
        }

        [Theory]
        [InlineData(1, 1, 1, 10, 1, 1, 1, 2, 2, 2, 10)]
        [InlineData(2, 2, 2, 4, 1, 1, 1, 5, 5, 5, 4)]
        public void Query_ValidParameters_ExpectedResult(long x, long y, long z, long value, long x1, long y1, long z1, long x2, long y2, long z2, long expectedResult)
        {
            InstanceToTest.Update(x, y, z, value);

            var result = InstanceToTest.Query(x1, y1, z1, x2, y2, z2);

            Assert.Equal(expectedResult, result);
        } 
    }
}
