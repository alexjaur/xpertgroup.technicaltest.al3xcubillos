﻿using System.Collections.Generic;
using System.Linq;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.IntegrationTest.Models;
using Xunit;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.IntegrationTest
{
    public class CubeSummationManagerTest
    {
        #region [ Setup ]

        private readonly ICubeSummationManager _cubeSummationManager;

        public CubeSummationManagerTest()
        {
            _cubeSummationManager = CubeSummationManagerFactory.Create();            
        }

        ~CubeSummationManagerTest()
        {
            _cubeSummationManager.Dispose();
        }
        
        public static IEnumerable<object[]> Run_MemberData
        {
            get
            {
                //
                // params: (IEnumerable<CubeTestCaseModel> dataForTest)
                //
                return new List<object[]>
                {
                    new object[]
                    {
                        new CubeTestCaseModel[]
                        {
                            //
                            // Case #1
                            //
                            new CubeTestCaseModel()
                            {
                                CubeSize = 5,
                                ExpectedResult = 10,
                                Updates = new CubeUpdateModel[]
                                {
                                    new CubeUpdateModel(1, 1, 1, 2),
                                    new CubeUpdateModel(2, 2, 2, 2),
                                    new CubeUpdateModel(3, 3, 3, 2),
                                    new CubeUpdateModel(4, 4, 4, 2),
                                    new CubeUpdateModel(5, 5, 5, 2),
                                    new CubeUpdateModel(2, 3, 4, 2),
                                    new CubeUpdateModel(4, 3, 2, 2),
                                },
                                Query = new CubeQueryModel(2, 2, 2, 4, 4, 4)
                            },

                            //
                            // Case #2
                            //
                            new CubeTestCaseModel()
                            {
                                CubeSize = 7,
                                ExpectedResult = 20,
                                Updates = new CubeUpdateModel[]
                                {
                                    new CubeUpdateModel(1, 2, 5, 10),
                                    new CubeUpdateModel(2, 3, 3, 10),
                                    new CubeUpdateModel(3, 4, 1, 10)
                                },
                                Query = new CubeQueryModel(1, 2, 3, 3, 3, 5)
                            },

                            //
                            // Case #3
                            //
                            new CubeTestCaseModel()
                            {
                                CubeSize = 10,
                                ExpectedResult = 1,
                                Updates = new CubeUpdateModel[]
                                {
                                    new CubeUpdateModel(1, 5, 5, -1),
                                    new CubeUpdateModel(2, 3, 3, -2),
                                    new CubeUpdateModel(3, 2, 4, -3),
                                    new CubeUpdateModel(4, 3, 5, 4),
                                    new CubeUpdateModel(3, 4, 1, 5)
                                },
                                Query = new CubeQueryModel(3, 2, 4, 4, 3, 5)
                            }
                        }                        
                    }
                };
            }
        }

        #endregion


        [Theory]
        [MemberData(nameof(Run_MemberData))]
        public void Run(CubeTestCaseModel[] dataForTest)
        {
            var totalTestCases = dataForTest.Count();
            var cubeTestCases = _cubeSummationManager.BuildCubeTestCases(totalTestCases);

            int index = 0;
            CubeTestCaseModel data = null;
            void CubeConfiguration(CubeData cubeData)
            {
                // set "data" by index
                data = dataForTest[index];

                // update "cubeData"
                cubeData.CubeSize = data.CubeSize;
                cubeData.Operations = data.Operations;
            }

            while (cubeTestCases.MoveNext(CubeConfiguration))
            {
                Assert.NotNull(data);

                var cube = cubeTestCases.Current;
                Assert.NotNull(cube);

                foreach (var update in data.Updates)
                {
                    cube.Update(update.X, update.Y, update.Z, update.Value);
                }

                var queryResult = cube.Query(data.Query.X1, data.Query.X2, data.Query.Y1, data.Query.Y2, data.Query.Z1, data.Query.Z2);
                Assert.Equal(data.ExpectedResult, queryResult);
            }
        }
    }
}
