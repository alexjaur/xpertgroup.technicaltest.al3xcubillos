﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.IntegrationTest.Models
{
    public class CubeUpdateModel
    {
        public CubeUpdateModel()
        { }

        public CubeUpdateModel(long x, long y, long z, long value)
        {
            X = x;
            Y = y;
            Z = z;
            Value = value;
        }

        public long X { get; set; }
        public long Y { get; set; }
        public long Z { get; set; }
        public long Value { get; set; }
    }
}
