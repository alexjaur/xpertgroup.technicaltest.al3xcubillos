﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.IntegrationTest.Models
{
    public class CubeQueryModel
    {
        public CubeQueryModel()
        { }
         
        public CubeQueryModel(long x1, long x2, long y1, long y2, long z1, long z2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
            Z1 = z1;
            Z2 = z2;
        }

        public long X1 { get; set; }
        public long X2 { get; set; }
        public long Y1 { get; set; }
        public long Y2 { get; set; }
        public long Z1 { get; set; }
        public long Z2 { get; set; }
    }
}
