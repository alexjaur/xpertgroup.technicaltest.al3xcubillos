﻿using System.Collections.Generic;
using System.Linq;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.IntegrationTest.Models
{
    public class CubeTestCaseModel
    {
        public IEnumerable<CubeUpdateModel> Updates { get; set; }
        public CubeQueryModel Query { get; set; }
        public long ExpectedResult { get; set; }
        public long CubeSize { get; set; }
        public long Operations => (Updates?.Count() ?? 0) + (Query != null ? 1 : 0);
    }
}
