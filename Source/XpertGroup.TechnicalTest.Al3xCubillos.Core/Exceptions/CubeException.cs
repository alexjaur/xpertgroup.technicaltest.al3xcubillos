﻿using System;
using System.Runtime.Serialization;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions
{
    [Serializable]
    public class CubeException : Exception
    {
        public CubeException()
        { }

        public CubeException(string message)
            : base(message)
        { }

        public CubeException(string message, Exception innerException)
            : base(message, innerException)
        { }

        protected CubeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
