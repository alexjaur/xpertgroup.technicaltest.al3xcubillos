﻿using System;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions
{
    [Serializable]
    public class OutOfRangeValueCubeException : CubeException
    {
        public OutOfRangeValueCubeException()
        { }

        public OutOfRangeValueCubeException(string dataName)
            : base($"Out of range value. Data: '{dataName}'")
        { }

        public OutOfRangeValueCubeException(string dataName, Exception innerException)
            : base($"Out of range value. Data: '{dataName}'", innerException)
        { }
    }
}
