﻿using SimpleInjector;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("XpertGroup.TechnicalTest.Al3xCubillos.Core.UnitTest")]

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    internal static class Configuration
    {
        internal static void RegisterCore(this Container container)
        {
            container.Register<ICubeTestBuilder, CubeTestBuilder>();
            container.Register<ICubeDataValidation, CubeDataValidation>();
        }
    }
}
