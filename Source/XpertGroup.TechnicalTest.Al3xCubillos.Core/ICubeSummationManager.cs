﻿using System;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public interface ICubeSummationManager : IDisposable
    {
        ICubeTestCases BuildCubeTestCases(long testCases);
    }
}
