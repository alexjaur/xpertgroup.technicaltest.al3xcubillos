﻿using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation
{
    class CubeTestBuilder : ICubeTestBuilder
    {
        private readonly ICubeDataValidation _dataValidation;


        public CubeTestBuilder(ICubeDataValidation dataValidation)
        {
            _dataValidation = dataValidation;
        }

        public ICubeTestCases Build(long testCases)
        {
            if (!_dataValidation.TestCases(testCases))
            {
                throw new OutOfRangeValueCubeException(nameof(testCases));
            }

            var cubeTestCases = new CubeTestCases(_dataValidation, testCases);

            return cubeTestCases;
        }
    }
}