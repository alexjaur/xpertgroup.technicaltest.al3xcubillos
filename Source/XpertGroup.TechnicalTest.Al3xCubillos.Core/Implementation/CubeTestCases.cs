﻿using System;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation
{
    class CubeTestCases : ICubeTestCases
    {
        private readonly ICubeDataValidation _dataValidation;

        private int Position { get; set; } = -1;
        public long TestCases { get; }
        public ICube Current { get; private set; }


        public CubeTestCases(ICubeDataValidation dataValidation, long testCases)
        {
            _dataValidation = dataValidation ?? throw new ArgumentNullException(nameof(dataValidation));

            TestCases = testCases;
        }


        public bool MoveNext(Action<CubeData> config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }

            Position++;

            var canBuildNext = CanBuildNext();
            if (canBuildNext)
            {
                var cubeData = new CubeData();
                config.Invoke(cubeData);

                Current = BuildCube(cubeData.CubeSize, cubeData.Operations);
            }
            else
            {
                Current = null;
            }
            
            return canBuildNext;
        }


        private bool CanBuildNext()
        {
            return Position > -1 && Position < TestCases;
        }

        private ICube BuildCube(long cubeSize, long operations)
        {
            if (!_dataValidation.CubeSize(cubeSize))
            {
                throw new OutOfRangeValueCubeException(nameof(cubeSize));
            }

            if (!_dataValidation.Operations(operations))
            {
                throw new OutOfRangeValueCubeException(nameof(operations));
            }

            var cube = new Cube(_dataValidation, cubeSize, operations);

            return cube;
        }
    }
}