﻿using System;
using XpertGroup.TechnicalTest.Al3xCubillos.Core.Exceptions;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation
{
    class Cube : ICube
    {
        private readonly ICubeDataValidation _dataValidation;
        
        public long CubeSize { get; }
        public long Operations { get; }
        private long[,,] Array3D { get; }
        internal long CurrentOperations { get; set; }


        public Cube(ICubeDataValidation dataValidation, long cubeSize, long operations)
        {
            _dataValidation = dataValidation ?? throw new ArgumentNullException(nameof(dataValidation));
            
            CubeSize = cubeSize;
            Operations = operations;

            Array3D = new long[CubeSize, CubeSize, CubeSize];
        }


        public void Update(long x, long y, long z, long value)
        {
            if (!CanExecuteNextOperation())
            {
                throw new CubeException("The number of operations has been exceeded");
            }

            if (!_dataValidation.NewValueIndex(CubeSize, x))
            {
                throw new OutOfRangeValueCubeException(nameof(x));
            }

            if (!_dataValidation.NewValueIndex(CubeSize, y))
            {
                throw new OutOfRangeValueCubeException(nameof(y));
            }

            if (!_dataValidation.NewValueIndex(CubeSize, z))
            {
                throw new OutOfRangeValueCubeException(nameof(z));
            }

            if (!_dataValidation.NewValue(value))
            {
                throw new OutOfRangeValueCubeException(nameof(value));
            }

            Array3D[x - 1, y - 1, z - 1] = value;

            CurrentOperations++;
        }

        public long Query(long x1, long y1, long z1, long x2, long y2, long z2)
        {
            if (!CanExecuteNextOperation())
            {
                throw new CubeException("The number of operations has been exceeded");
            }

            if (!_dataValidation.IndexesOfQuery(CubeSize, x1, x2))
            {
                throw new OutOfRangeValueCubeException($"{nameof(x1)} - {nameof(x2)}");
            }

            if (!_dataValidation.IndexesOfQuery(CubeSize, y1, y2))
            {
                throw new OutOfRangeValueCubeException($"{nameof(y1)} - {nameof(y2)}");
            }

            if (!_dataValidation.IndexesOfQuery(CubeSize, z1, z2))
            {
                throw new OutOfRangeValueCubeException($"{nameof(z1)} - {nameof(z2)}");
            }

            long result = 0,
                xMin = x1 - 1,
                xMax = x2 - 1,
                yMin = y1 - 1,
                yMax = y2 - 1,
                zMin = z1 - 1,
                zMax = z2 - 1;

            for (long x = xMin; x <= xMax; x++)
            {
                for (long y = yMin; y <= yMax; y++)
                {
                    for (long z = zMin; z <= zMax; z++)
                    {
                        result += Array3D[x, y, z];
                    }
                }
            }

            CurrentOperations++;

            return result;
        }

        public bool CanExecuteNextOperation()
        {
            return _dataValidation.CanExecuteOperation(Operations, CurrentOperations);
        }        
    }
}