﻿using System;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation
{
    class CubeDataValidation : ICubeDataValidation
    {
        /*
            1 <= T <= 50        --> TestCases
            1 <= N <= 100       --> CubeSize
            1 <= M <= 1000      --> Operations
            1 <= x1 <= x2 <= N  --> IndexesOfQuery
            1 <= y1 <= y2 <= N  --> IndexesOfQuery
            1 <= z1 <= z2 <= N  --> IndexesOfQuery
            1 <= x,y,z <= N     --> 
            -10^9 <= W <= 10^9  --> NewValue
        */

        private readonly long _maxNewValue;
        private readonly long _minNewValue;


        public CubeDataValidation()
        {
            _maxNewValue = (long)Math.Pow(10, 9);
            _minNewValue = _maxNewValue * -1;
        }


        public bool TestCases(long value) 
            => 1 <= value && value <= 50;

        public bool CubeSize(long value) 
            => 1 <= value && value <= 100;

        public bool Operations(long value) 
            => 1 <= value && value <= 1000;

        public bool IndexesOfQuery(long cubeSize, long index1, long index2) 
            => 1 <= index1 && index1 <= index2 && index2 <= cubeSize;

        public bool NewValue(long value) 
            => _minNewValue <= value && value <= _maxNewValue;

        public bool NewValueIndex(long cubeSize, long index) 
            => 1 <= index && index <= cubeSize;

        public bool CanExecuteOperation(long maxOperations, long currentOperations) 
            => 1 <= maxOperations && currentOperations < maxOperations;
    }
}