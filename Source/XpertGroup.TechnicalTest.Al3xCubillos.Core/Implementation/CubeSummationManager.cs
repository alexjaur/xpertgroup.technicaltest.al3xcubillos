﻿using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation
{
    class CubeSummationManager : ICubeSummationManager, IDisposable
    {
        internal Lazy<Scope> Scope { get; }
        internal Container Container => Scope.Value.Container;


        public CubeSummationManager()
        {
            Scope = new Lazy<Scope>(BuildScopeBase);
        }


        protected virtual Scope BuildScopeBase()
        {
            // container
            var container = new Container();

            // container options
            container.Options.DefaultLifestyle = Lifestyle.Scoped;
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            // container options
            container.RegisterCore();

            // verify container
            container.Verify();

            // scope
            return AsyncScopedLifestyle.BeginScope(container);
        }
        
        public virtual ICubeTestCases BuildCubeTestCases(long testCases) 
        {
            var cubeTestBuilder = Container.GetInstance<ICubeTestBuilder>();
            var cubeTestCases = cubeTestBuilder.Build(testCases);

            return cubeTestCases;
        }


        #region [ IDisposable ]

        protected bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Scope?.Value?.Dispose();
                }

                _disposed = true;
            }
        }

        ~CubeSummationManager()
        {
            Dispose(false);
        }

        #endregion
    }
}
