﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public interface ICubeTestBuilder
    {
        ICubeTestCases Build(long testCases);
    }
}