﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public interface ICubeDataValidation
    {
        bool TestCases(long value);
        bool CubeSize(long value);
        bool Operations(long value);
        bool CanExecuteOperation(long maxOperations, long currentOperations);
        bool IndexesOfQuery(long cubeSize, long index1, long index2);
        bool NewValue(long value);
        bool NewValueIndex(long cubeSize, long index);
    }
}