﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string @string)
        {
            return !string.IsNullOrWhiteSpace(@string);
        }
    }
}
