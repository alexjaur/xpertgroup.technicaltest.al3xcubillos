﻿using System;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public interface ICubeTestCases
    {
        long TestCases { get; }
        ICube Current { get; }
        bool MoveNext(Action<CubeData> config);
    }
}