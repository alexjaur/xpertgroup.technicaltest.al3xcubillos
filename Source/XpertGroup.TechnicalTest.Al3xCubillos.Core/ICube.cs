﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public interface ICube
    {
        long CubeSize { get; }
        long Operations { get; }

        void Update(long x, long y, long z, long value);
        long Query(long x1, long y1, long z1, long x2, long y2, long z2);
        bool CanExecuteNextOperation();
    }
}