﻿using XpertGroup.TechnicalTest.Al3xCubillos.Core.Implementation;

namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public class CubeSummationManagerFactory
    {
        public static ICubeSummationManager Create()
        {
            return new CubeSummationManager();
        }
    }
}
