﻿namespace XpertGroup.TechnicalTest.Al3xCubillos.Core
{
    public class CubeData
    {
        public long CubeSize { get; set; }
        public long Operations { get; set; }
    }
}
