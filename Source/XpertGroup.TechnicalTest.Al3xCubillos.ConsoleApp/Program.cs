﻿using System;
using System.Text.RegularExpressions;
using XpertGroup.TechnicalTest.Al3xCubillos.Core;

namespace XpertGroup.TechnicalTest.Al3xCubillos.ConsoleApp
{
    class Program
    {
        static long CurrentTestCase { get; set; }
        static ICubeTestCases CubeTestCases { get; set; }


        static void Main(string[] args)
        {
            try
            {
                using (var cubeSummationManager = CubeSummationManagerFactory.Create())
                {
                    Console.Write("Number Of Tests: ");
                    var testCases = GetValue();
                    Console.Clear();

                    CubeTestCases = cubeSummationManager.BuildCubeTestCases(testCases);

                    while (CubeTestCases.MoveNext(CubeConfiguration))
                    {
                        var cube = CubeTestCases.Current;

                        while (cube.CanExecuteNextOperation())
                        {
                            Console.Write(" Command: ");
                            var cmd = Console.ReadLine();

                            var validCommand = ExecuteCommand(cmd);
                            if (!validCommand)
                            {
                                Console.WriteLine(" Invalid Command!");
                            }

                            Console.WriteLine();
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("--- Error ---");
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }


        static bool ExecuteCommand(string cmd) 
        {
            bool result = false;
            var updatePattern = @"^(?<X>\d+)\s(?<Y>\d+)\s(?<Z>\d+)\s(?<Value>\d+)$";
            var queryPattern = @"^(?<X1>\d+)\s(?<Y1>\d+)\s(?<Z1>\d+)\s(?<X2>\d+)\s(?<Y2>\d+)\s(?<Z2>\d+)$";

            var matches = new Regex(updatePattern).Matches(cmd);
            if (matches.Count > 0)
            {
                long
                    x = long.Parse(matches[0].Groups["X"].Value),
                    y = long.Parse(matches[0].Groups["Y"].Value),
                    z = long.Parse(matches[0].Groups["Z"].Value),
                    value = long.Parse(matches[0].Groups["Value"].Value);

                CubeTestCases.Current.Update(x, y, z, value);

                result = true;
            }
            else
            {
                matches = new Regex(queryPattern).Matches(cmd);
                if (matches.Count > 0)
                {
                    long
                        x1 = long.Parse(matches[0].Groups["X1"].Value),
                        y1 = long.Parse(matches[0].Groups["Y1"].Value),
                        z1 = long.Parse(matches[0].Groups["Z1"].Value),
                        x2 = long.Parse(matches[0].Groups["X2"].Value),
                        y2 = long.Parse(matches[0].Groups["Y2"].Value),
                        z2 = long.Parse(matches[0].Groups["Z2"].Value);

                    var queryResult = CubeTestCases.Current.Query(x1, y1, z1, x2, y2, z2);
                    Console.WriteLine($" Query Result: {queryResult}");

                    result = true;
                }
            }

            return result;
        }

        static void CubeConfiguration(CubeData cubeData)
        {
            CurrentTestCase++;

            Console.WriteLine();
            Console.WriteLine("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
            Console.WriteLine($"■■   Test Cases {CurrentTestCase}/{CubeTestCases.TestCases}");
            Console.WriteLine("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
            Console.WriteLine();

            Console.Write(" Cube Size: ");
            cubeData.CubeSize = GetValue();

            Console.Write(" Operations: ");
            cubeData.Operations = GetValue();
            Console.WriteLine();
        }

        static long GetValue()
        {
            if (!long.TryParse(Console.ReadLine(), out long result))
            {
                throw new InvalidCastException();
            }

            return result;
        }
    }
}
